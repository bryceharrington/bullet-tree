# bullet-tree

Code to parse, write, and manage nested bullet lists.

This module is specifically designed for parsing and writing Debian
changelog entries, but is also applicable to managing todo lists, status
reports, and so on.

It is not intended for use as config files or program data files (use
YAML or various other file format for that), nor is intended to be used
for wiki-like content management (use Markdown).
