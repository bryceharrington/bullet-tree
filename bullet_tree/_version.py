# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

__version__ = "0.1.0"
__version_info__ = tuple(map(int, __version__.split('.')))
