#!/usr/bin/env python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
#
# Copyright (c) 2019 - Bryce W. Harrington
#
# bullet-tree - A bullet list parser and writer

import re
import glob

from setuptools import setup, find_packages

def get_version(package):
    """Directly retrieve version, avoiding an import

    Since setup.py runs before the package is set up, we can't expect
    that simply doing an import ._version will work reliably in all
    cases.  Instead, manually import the version from the file here,
    and then the module can be imported elsewhere in the project easily.
    """
    version_file = "%s/%s" %(package, '_version.py')
    version_string = open(version_file, "rt").read()
    re_version = r"^__version__ = ['\"]([^'\"]*)['\"]"
    m = re.search(re_version, version_string, re.M)
    if not m:
        raise RuntimeError("Unable to find version string for %s in %s." %(
            package, version_file))
    return m.group(1)

def get_description():
    return open('README.md', 'rt').read()

setup(
    name             = 'bullet-tree',
    version          = get_version('bullet-tree'),
    url              = 'https://gitlab.com/bryceharrington/bullet-tree',
    download_url     = 'none',
    author           = 'Bryce W. Harrington',
    author_email     = 'bryce@canonical.com',
    description      = 'Nested bullet list management',
    long_description = get_description(),
    keywords         = [ 'bullet', 'list', 'tree', 'changelog' ],
    classifiers      = [
        # See https://pypi.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Information Technology',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Text Processing :: Markup',
        'Topic :: Text Editors :: Text Processing',
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        ],
    platforms        = ['any'],
    python_requires  = '>=3',
    setup_requires   = [
        'pytest-runner'
        ],
    tests_require    = [
        'pytest'
        'pep8',
        'pyflakes',
        ],
    install_requires = [
        'argparse',
        ],

    packages         = find_packages(),
    package_data     = { },
    data_files       = [ ],
    scripts          = glob.glob('scripts/*'),
)
