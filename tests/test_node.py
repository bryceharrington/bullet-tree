#!/usr/bin/env python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

import os
import sys
import pytest

sys.path.insert(0, os.path.realpath(
    os.path.join(os.path.dirname(__file__), "..")))

from bullet_tree.node import Node

def test_object_create():
    pass

def test_list_init():
    todo_list = """
* item a
* item b
  - subitem 0
  - subitem 1
    + detail foo
    + detail bar
  - subitem 2
"""

@pytest.mark.parametrize('foo,bar,expected_result', [
    (None, None, False),
])
def test_tree(foo, bar, expected_result):
    t = Node('* root', [
        Node('  - item', [
            Node('    + bar'),
            Node('    + baz')])
            ])
    print(t)
