#!/usr/bin/env python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

import sys, os.path
import pytest
from bullet_tree._version import __version__, __version_info__


class TestVersion(object):
    def test_version(self):
        assert(type(__version__) is str)
        assert('.' in __version__)
        assert(__version__[0].isdigit())
        assert(__version__[-1] != '.')

    def test_version_info(self):
        assert(type(__version_info__) is tuple)
        assert(len(__version_info__) > 1)
        for elem in __version_info__:
            assert(type(elem) is int)
            assert(elem >= 0)
